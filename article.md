Property-based testing : Annexer des tests au contrat d'interface
=================================================================

_La compréhension de cet article est facilitée par des connaissances sur
[l'architecture hexagonale (Clean Archi)](https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/)
et le [Domain-Driven Design](https://www.cafe-craft.fr/4)._

Lorsque vous développez un produit en vous basant sur les principes du _Domain-Driven Design_ (DDD) et que vous vous efforcez
de respecter les principes de _Clean Archi_, vous vous retrouvez alors probablement avec une catégorie particulière d'interfaces
appelées [_Repository_](https://martinfowler.com/eaaCatalog/repository.html).
Nous allons voir ici qu'une stratégie de test des implémentations se reposant uniquement
sur les méthodes de l'interface peut s'avérer très utile pour itérer sur notre implémentation sans influencer
notre code métier.

Que fait un _repository_ ?
--------------------------

Pour les besoins de notre article, nous allons nous focaliser sur la forme la plus simple d'un _repository_. Cette forme
est tout de même pertinente car dans une optique de _Clean Architecture_, on essaiera de localiser au maximum la complexité
dans le code métier, c'est à dire les méthodes de l'agrégat ou les _Use Cases_. En général, le _repository_ se retrouve
alors à ne gérer que l'enregistrement unitaire d'un agrégat, les agrégats étant indépendants entre eux. On considère alors
un agrégat comme une unité de persistence. Si on se dirige vers une approche CQRS, alors les _repositories_ peuvent également
perdre en complexité de requêtage. Toutes ces approches visent justement à simplifier l'implementation des interfaces
qui sont spécifiques à l'infrastructure afin de localiser la complexité métier dans du code qui ne dépend pas d'autre systèmes
et qu'on pourra alors [tester unitairement](https://blog.octo.com/un-test-peut-en-cacher-un-autre%E2%80%8A-%E2%80%8Atests-unitaires%E2%80%8A-%E2%80%8Ap1/)
plutôt qu'avec des [tests d'intégration](https://blog.octo.com/un-test-peut-en-cacher-un-autre-tests-dintegration-p2/).

J'utiliserai des exemples en TypeScript, mais les principes s'appliquent à beaucoup de langages.
Imaginons un _repository_ pour un agrégat `Machin`.

```typescript
interface MachinRepository {
  get (id: Machin.Id): Promise<Machin | undefined>
  save (machin: Machin): Promise<void>
  remove (id: Machin.Id): Promise<void>
}
```

Ici, nous avons un genre de forme canonique de repository. Notre typage nous informe déjà de quelques indices importants
pour l'implémentation de notre interface :

+ les méthodes exposées sont asynchrones, c'est à dire qu'elle font des entrées / sorties. L'univers javascript nous
  force à préciser ceci, mais il peut être utile aussi dans votre langage de prédilection de différencier par leur type les
  fonctions qui font appel à d'autres systèmes (disque, réseau, etc.)
+ Enregistrer avec `save()` ne nécessite aucune valeur de retour car le stockage n'a pas d'influence sur le contenu
  ou l'état de l'agrégat
+ l'identifiant de notre agrégat `id` fait partie de son contenu, il n'est donc pas nécessaire de le préciser à la sauvegarde
+ Lire un agrégat par son `id` peut avoir deux résultats distincts (hors erreurs).

Ces indices sont déjà très utile à un _type checker_ lorsque vous utiliserez l'interface. Ils vous indiquent aussi
certaines choses à garder en tête lors de l'implémentation, mais pas toutes ! Voici des éléments qui n'apparaissent pas
dans la déclaration de l'interface et qui sont pourtant indispensable au bon fonctionnement d'un _repository_ :

+ Avec aucun contenu enregistré, `get()` renvoie toujours `undefined`
+ Si j'appelle `save(machin1)` et ensuite `machin2 = get(machin1.id)`, alors `machin1` == `machin2`
+ Si j'appelle `save(machin)`, puis `remove(machin.id)` et enfin `get(machin.id)`, alors j'obtiens toujours `undefined`
+ l'ordre d'appel de `save(a)` et `save(b)` n'a pas de conséquence sur les valeurs stockées de `a` et `b`
+ La liste continue…

Ces comportements peuvent nous paraître évident parce que nous baragouinons l'anglais et nous n'en sommes pas à notre
premier _repository_, mais il faut reconnaître que ces attentes vis-à-vis de `MachinRepository` sont tout à fait implicites.
En développeurs expérimentés, nous comprenons alors que des attentes implicites sont le terreau fertile des incompréhensions,
et que les incompréhensions, ça tend les relations. Les relations tendues, c'est mauvais pour la santé. Il nous faut
préserver notre santé en limitant les attentes implicites !

Un autre enjeu de rendre ces _propriétés_ explicites, c'est de transformer l'essai sur la définitions d'interfaces pour
nos classes d'infrastructure. En effet, si toutes les propriétés de notre _repository_ sont connues, alors il sera
plus simple d'en changer l'implementation. On pourra passer d'une implementation _in-memory_ pour les toutes premières itérations
à un implémentation avec Sqlite puis ensuite avec une base de données Oracle™ par exemple. On voudra que chacune de ces
générations d'implémentations possède ces mêmes _propriétés_ qui garantissent sa bonne utilisation depuis le code métier.

Tester le comportement de l'interface
------------------------------------

Pour rendre explicite des comportements implicites, rien de tel que des tests automatisés ! Voyons ici avec une syntaxe
Mocha / Chai

```typescript
describe('MachinRepository', () => {
  let repository: MachinRepository
  beforeEach(async () => {
    repository = await setupMachinRepository()
  })
  afterEach(() => teardownMachinRepository(repository))

  context("quand rien n'est enregistré", () => {
    describe('.get(id)', () => {
      it('résoud undefined', async () => {
        // Given
        const id = '123'
        // When
        const actual = await repository.get(id)
        // Then
        expect(actual).to.equal(undefined)
      })
    })
  })

  context('quand un machin est enregistré', () => {
    const machin = unMachin()
    beforeEach(() => repository.save(machin))

    describe('.get(machin.id)', () => {
      it('résoud le même machin', async () => {
        // When
        const actual = await repository.get(machin.id)
        // Then
        expect(actual).to.deep.equal(machin)
      })
    })

    describe('.get(autreId)', () => {
      it('résoud undefined', async () => {
        // When
        const actual = await repository.get('456')
        // Then
        expect(actual).to.equal(undefined)
      })
    })

    describe("et qu'on appelle .remove(machin.id)", () => {
      beforeEach(() => repository.remove(machin.id))

      describe('.get(machin.id)', () => {
        it('résoud undefined', async () => {
          // When
          const actual = await repository.get(machin.id)
          // Then
          expect(actual).to.equal(undefined)
        })
      })
    })
  })
})
```

Ce qui donne cette magnifique éxecution :

```plaintext
MachinRepository
  quand rien n'est enregistré
    .get(id)
      résoud undefined ✓
  quand un machin est enregistré
    .get(machin.id)
      résoud le même machin ✓
    .get(autreId)
      résoud undefined ✓
    et qu'on appelle .remove(machin.id)
      .get(machin.id)
        résoud undefined ✓
```

L'exemple n'est pas exhaustif, mais ces quelques tests explicitent très bien quelques points clés du comportement de notre
_repository_ :

+ On lit exactement ce qu'on vient d'y écrire
+ Pas de distinction possible entre _juste supprimé_ et _jamais enregistré_
+ Les agrégats sont indépendants entre eux dans leur stockage

On peut imaginer d'autres _propriétés_ qu'il serait intéressant de vérifier, mais j'essaye de garder les exemples courts.
La valeur de ces quelques tests est que nous pourrons alors vérifier ces _propriétés_ pour **toutes les implementations**
de notre interface.

Voici quelques particularités que l'on peut remarquer au sujet de la stratégie de test

### Injection du _System Under Test_

Le sujet de nos tests ici s'appelle `repository` et il n'est typé que par son interface `MachinRepository`.

```typescript
let repository: MachinRepository
beforeEach(async () => {
  repository = await setupMachinRepository()
})
afterEach(() => teardownMachinRepository(repository))
```

j'ai omis la définition des fonctions `setupMachinRepository()` et `teardownMachinRepository()` parce que leur
implémentation dépend de la classe que vous voulez instancier et sur une instance de laquelle vous
voulez lancer ces tests. Par exemple, avec une implémentation sur système de fichier :

```typescript
async function setupMachinRepository () {
  const directory = await fs.mkdtemp()
  return new FileSystemMachinRepository(directory)
}

async function teardownMachinRepository (repository: FileSystemMachinRepository) {
  await repository.cleanDirectory()
}
```

L'idée sous-jacente est que l'initialisation et le nettoyage du _system under test_ soit injectable et qu'aucune
autre information ne soit nécessaire à son utilisation que l'implémentation de la bonne interface.

En forme complète, on peut imaginer que ce test ressemble à ceci :

```typescript
describeMachinRepository(setupFileRepository, teardownFileRepository)

function describeMachinRepository (setup, teardown) {
  describe('MachinRepository', () => {
    let repository: MachinRepository
    beforeEach(async () => {
      repository = await setup()
    })
    afterEach(() => teardown(repository))
    
    // ... tests ...
  })
}
```

### Sacrilège ! des méthodes de l'interface en _setup_

En effet, nos testent mélangent l'utilisation des différentes méthodes de l'interface
plutôt que de se focaliser sur une méthode en particulier. Cela peut paraître iconoclaste mais c'est
nécessaire dans notre cas. En effet, la fonction principale de notre _repository_ est de gérer
des effets de bords : l'enregistrement. Ce que l'on cherche à tester au niveau des _propriétés_ de notre
interface, c'est bien la gestion de ces effets de bords et comment les différentes méthodes s'influencent
les unes et les autres. Notamment, on cherche à décrire comment se comporte notre _repository_ en fonction
de l'ordre d'appel de ses méthodes.

```typescript
await repository.save(machin)
await repository.remove(machin.id)
```

N'a pas les mêmes conséquences que

```typescript
await repository.remove(machin.id)
await repository.save(machin)
```

dans les exemples précédents, j'ai utilisé certaines méthodes de l'interface dans `beforeEach()` (c'est à dire la partie
_Given_ de mon test). Ceci résulte de la manière dont j'ai structuré mes tests avec `describe`, `context` et `it` qui
sont taillés principalement pour le test unitaire d'une méthode en particulier. Ce que l'on cherche réellement à tester
ici, c'est les conséquences d'une série d'appels de notre _repository_. C'est donc normal que nous appelions plusieurs
méthodes à la fois dans nos tests.


### Assertions sur la lecture

En conséquence du point précédent, on remarque aussi que les assertions se font uniquement sur le résultat
de notre méthode `get(id)`. Du point de vue de notre interface, la méthode `get(id)` est la seule qui n'a pas d'effet
de bord (ce qui ne sera pas forcément vrai pour son implémentation). Appeler `get(id)` n'a aucun effet sur les agrégats déjà
enregistrés et on peut l'appeler plusieurs fois de suite sans résultat différent. C'est également la seule chose
que l'on peut observer, les autres méthodes renvoyant `Promise<void>`.

Dans des tests d'intégration classiques, sur la classe d'implémentation, on vérifiera par exemple qu'un fichier a bien
été écrit, qu'une insertion SQL a bien été lancée ou même qu'un requête HTTP est partie. Tout ceci relève bien de
l'implémentation mais **ce n'est pas le comportement de l'interface**, ce n'est pas l'attente de l'utilisateur de l'interface.
C'est une manière parmis d'autre d'atteindre ce comportement, mais ce n'est pas par cela qu'on le décrit.
Ici, donc, on décrit les propriétés de l'interface en ne considérant aucun système externe, réellement en isolation.

Le comportement attendu de l'interface, quelque soit sont implémentation, c'est qu'on pourra par exemple y lire
ce qu'on vient d'y écrire. Si on ne veut tester que les propriétés propres de l'interface, alors on teste en
utilisant `get()` !

Des tests complémentaires, spécifiques à l'implémentation, seront toujours de mise. En effet, il est intéressant de tester
comment une implémentation se comporte avec le système de fichiers, comment elle s'authentifie auprès d'un autre service,
_etc._


> J'utilise volontairement un langage dont le typage est limité, j'aurais pu utiliser Java, Kotlin ou C++ qui sont
> également des langages qui échouent à décrire la relation que l'on peut avoir entre `save()` et `get()`. L'objet
> de cet article est d'annexer à la déclaration de l'interface une suite de tests pour compléter sa description.
> Notez qu'avec un langage qui ne peut déclarer une interface, comme Javascript par exemple, vous pourrez alors toujours
> vous reposer uniquement sur une suite de tests pour la décrire. Cette discipline de décrire les contrats interfaces par
> des tests se retrouve dans la littérature sous le nom de [_Contract Testing_](https://blog.thecodewhisperer.com/permalink/a-real-life-contract-test)
> ou [_Abstract Test Case_](http://wiki.c2.com/?AbstractTestCases).

En route pour du Property-Based Testing
---------------------------------------

Si vous avez l'œil, vous aurez remarqué que j'ai volontairement mis en exergue jusqu'à maintenant le mot _propriété_.
Car ce que nous sommes en train de décrire s'en rapproche étrangement. Dans les exemples précédents j'ai utilisé une
méthode que je n'ai pas détaillée `unMachin()` qui retourne un agrégat de type `Machin`. Comme tous les tests
que nous avons l'habitude d'écrire, ils sont construits sur en ensemble _d'exemples_. `unMachin()` nous fabrique
un _exemple_ ou un _exemplaire_ de `Machin` que nous pouvons ensuite utiliser dans des _exemples_ de scénarios
d'utilisation.

Lorsque l'on parle de _propriétés_ d'un système, on s'attend en revanche à ce que son comportement et ses effets soient
vrais pour _toutes les valeurs possibles de son domaine_. Dans notre cas, cela voudra dire qu'on veut tester que le code
suivant est vrai pour _toutes les variantes possibles d'un `Machin`_ et pas seulement celles fabriquées par `unMachin()`.

```typescript
describe('MachinRepository', () => {
  context('quand on appelle .save(machin)', () => {
    describe('.get(machin.id)', () => {
      it('résoud le même machin', async () => {
        // Given
        const machin = unMachin()
        await repository.save(machin)
        // When
        const actual = await repository.get(machin.id)
        // Then
        expect(actual).to.deep.equal(machin)
      })
    })
  })
})
```

Le principe du [_property-based testing_](https://romainberthon.blog/2019/01/08/a-la-decouverte-du-property-based-testing/),
c'est de décrire le domaine des variantes possibles de `Machin` et de s'assurer
que nos attentes, les _propriétés_, sont respectées pour l'ensemble de ce domaine. On parle parfois également _d'invariants_.

En pratique, on va plutôt génerer un grand nombre d'exemples représentatifs de `Machin` et executer un même test
pour chacun d'entre eux. Allons-y petit à petit :

```typescript
describe('MachinRepository', () => {
  const exemples = [unMachin(), unMachinAvecUnTruc(), unMachinAvec2Trucs(), unMachinVide()]
  for (const machin of exemples) {
    context('quand on appelle .save(machin)', () => {
      describe('.get(machin.id)', () => {
        it('résoud le même machin', async () => {
          // Given
          await repository.save(machin)
          // When
          const actual = await repository.get(machin.id)
          // Then
          expect(actual).to.deep.equal(machin)
        })
      })
    }
  })
})
```

Dans ce code, on génère des exemples avec des fonctions utilitaires et pour chacun d'entre eux on execute notre test.
On vérifie donc que, quelque soit la forme de notre `machin`, le _repository_ enregistre exactement ce `machin` et il
est capable de le restituer à l'identique. C'est tout ce qu'on attendait de notre _repository_ !

Écrire ces fonctions utilitaires peut être fastidieux. Gardez en tête que rien ne vous oblige à générer exactement toutes
les valeurs possibles. L'intuition du TDD reste de trouver _intelligemment_ des exemples qui font avancer la conception.
Le _property-based testing_ en revanche vise une description plus formelle du fonctionnement de notre code, elle permet
principalement d'éviter des conversations. Dosez donc votre évitement et restez pragramatique !

Si vous voulez tout de même utiliser un grand nombre d'exemples, alors il vous faudra automatiser la génération de ces
exemples, en gardant un minimum de cohérence. Dans cet exemple, je vais utiliser le module [`faker`](https://www.npmjs.com/package/faker),
et surtout je vais pour la première fois supposer ce que contient un `Machin`

```typescript
function *exemples (count=10) {
  while (0 < count--) {
    const nombreDeTrucs = Math.floor(Math.random() * 5)
    const machin: Machin = {
      id: faker.random.uuid(),
      nom: faker.name.title(),
      trucs: count(nombreDeTrucs).map(() => ({
        couleur: faker.commerce.color()
      }))
    }
    yield machin
  }
}

describe('MachinRepository', () => {
  for (const machin of exemples(100)) {
    context('quand on appelle .save(machin)', () => {
      describe('.get(machin.id)', () => {
        it('résoud le même machin', async () => {
          await repository.save(machin)
          const actual = await repository.get(machin.id)
          expect(actual).to.deep.equal(machin)
        })
      })
    }
  })
})
```

Voici qui devrait faire ! En générant une centaine d'exemples et en vérifiant le comportement de `save()` et `get()`,
nous pouvons être certains que toutes les implémentations qui valident ces tests respectent bien la promesse de notre
_repository_.

Vous pouvez adopter cette approche si vous vous attendez à de plusieurs implémentations de votre interface, des
versions incrémentales d'une même implémentation ou des implémentations par des personnes encore inconnues,
avec lesquelles vous n'êtes pas certains de pouvoir avoir les conversations nécessaires à la bonne compréhension
de notre interface.

> Le [_Property-Based Testing_](https://medium.com/criteo-labs/introduction-to-property-based-testing-f5236229d237) ne se
> limite évidemment pas aux _repositories_ ni mêmes aux interfaces. Même si ces notions proviennent de la programmation
> fonctionnelle et plutôt formelle, les principes sont transposables à des langages impératifs, typés ou non.


Conclusion
----------

Nous avons vu que la plupart des langages ne permettent qu'une description lacunaire de nos interfaces, notamment en ce qui
concerne les _Repositories_. En particulier, les interactions entre les différentes méthodes d'un _repository_ et leurs
effets de bord n'y sont pas décrites. Il peut être alors utile d'annexer à la déclaration du type une suite de tests
qui permet de préciser les attentes à son sujet. La ou les implémentations devront alors, en plus de leur suite de tests
propre si nécessaire, valider cette suite de tests formelle. Ceci s'appelle du [_Contract Testing_](http://wiki.c2.com/?AbstractTestCases)

Par exemple, j'ai pu utiliser cette approche sur une base de code en développant pour certains _repositories_ une implémentation
qui vise PostgreSQL pour la production et une implémentation en mémoire pour le développement. Cela nous a permis de tester
nos _Use Cases_ et notre domaine sans mocker ou stubber nos _repositories_. Les assertions se faisant alors sur le
contenu du repository à la fin du test plutôt que sur les appels faits à celui-ci. Nos tests étaient alors bien moins
dépendants de l'implémentation et surtout plus faciles à comprendre. Ces 2 implémentations, SQL et en mémoire, valident
chacune la suite de tests basée sur les méthodes de l'interface uniquement. Cette suite de tests à été construite en TDD
sur l'implémentation en mémoire d'abord, qui a d'ailleurs été déployée dans un premier temps avant que l'implémentation
SQL ne viennent prendre le relai.

En poussant le raisonnement plus loin et en constatant que nos attentes sont des _propriétés_ de notre _repository_, on
peut alors s'orienter vers du [_property-based testing_](https://dev.to/jdsteinhauser/intro-to-property-based-testing-2cj8)
pour couvrir un maximum de bases. Les tests ainsi réalisés sur une implémentation valideront qu'elle possède les
propriétés attendues et ses tests d'intégration spécifiques pourront alors se focaliser sur le détail de son
interaction avec un système externe (fichiers, base de données, index, _etc._)

Ces approches permettent _in fine_ de s'assurer que l'utilisation que nous faisons de nos _repositories_ dans notre code
métier, notre domaine, est garantie et indépendante de leur implémentation. Ceci nous permet de changer plus facilement
nos implémentations et donc de commencer avec des implémentations simplistes au début, pour valider les principales
hypothèses métier, puis plus complexes ensuite pour industrialiser notre produit. C'est un des aspects de la méthode
[_Accelerate_](https://blog.octo.com/compte-rendu-matinale-accelerate-la-vitesse-conditionne-lexcellence-un-nouveau-paradigme-dans-le-developpement-logiciel/)


### Pour aller plus loin…

#### Contrat Testing

+ [Abstract Test Cases](http://wiki.c2.com/?AbstractTestCases)
+ [A real-life contract test](https://blog.thecodewhisperer.com/permalink/a-real-life-contract-test)

#### Property-Based Testing

+ [Introduction to Property-Based testing](https://medium.com/criteo-labs/introduction-to-property-based-testing-f5236229d237)
+ [A la découverte du property-based testing](https://romainberthon.blog/2019/01/08/a-la-decouverte-du-property-based-testing/)
+ [Intro to Propertry-based testing](https://dev.to/jdsteinhauser/intro-to-property-based-testing-2cj8)
+ [In praise of property-based testing](https://increment.com/testing/in-praise-of-property-based-testing/)
