.PHONY: help

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: html static illustrations ## Build landing pages inside ./dist/

html: dist/index.html
static: dist/assets

dist/index.html: ./article.md
	@mkdir -p dist
	echo '' > /tmp/article.tmp
	npx marked -o /tmp/article.tmp article.md
	echo > dist/index.html '<html>'
	echo >> dist/index.html '<head>'
	echo >> dist/index.html  '<link rel="stylesheet" href="./assets/style.css">'
	echo >> dist/index.html '<link rel="stylesheet" href="./assets/prism.css">'
	echo >> dist/index.html '<script src="./assets/prism.js" defer></script>'
	echo >> dist/index.html '</head>'
	echo >> dist/index.html '<body>'
	cat  >> dist/index.html /tmp/article.tmp
	echo >> dist/index.html '</body>'
	echo >> dist/index.html '</html>'

IMG_IMG := $(wildcard images/*.png)
DIST_IMG := $(patsubst images/%.png,dist/%.png,$(IMG_IMG))
illustrations: $(DIST_IMG)

dist/assets: assets/*
	@mkdir -p dist/assets
	rm -rf dist/assets/*
	cp -r assets/* dist/assets

dist/%.png: images/%.gv
	@mkdir -p dist
	cp $< $@

clean: ## Remove dist
	rm -rf dist
